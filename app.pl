#!/usr/bin/env perl
use Mojolicious::Lite;

get '/' => sub{
	my $c = shift;
	$c->render(text => "This is homepage, use /download?url=\$url");
};

get '/download' => sub {
	my $c    = shift;
	my $url = $c->param('url');
	# $c->render(text => "youtube URL is: $url.");
	#my @args = ("youtube-dl-mp3","$url");
	#my @args = ("youtube-dl --extract-audio --audio-format mp3 $url");
	#system(@args) == 0 or die "system @args failed: $?";
	#chomp(my $file = `youtube-dl --get-filename $url`);
	#$file =~ s/mp4/mp3/g;
	#$c->render_file('filepath' => "$file");
	my @args = ("youtube-dl --extract-audio --no-warnings --get-url --audio-quality 0 $url");
	my $re = `@args`;
	$c->render(text => $re);	
};

app->start;
